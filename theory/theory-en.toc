\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Definition of Skew-Laurent Series and Examples}{4}
\contentsline {subsection}{\numberline {2.1}Definition}{4}
\contentsline {subsection}{\numberline {2.2}Examples of Skew-Laurent Series}{4}
\contentsline {subsubsection}{\numberline {2.2.1}The usual formal Laurent series ring over commutative ring $R$}{5}
\contentsline {subsubsection}{\numberline {2.2.2}The ring of pseudo-differential operators ($\Psi $DO)}{5}
\contentsline {subsubsection}{\numberline {2.2.3}The ring of difference operators ($\Delta $DO)}{5}
\contentsline {subsubsection}{\numberline {2.2.4}The ring of q-deformed differential operators (q-$\Psi $DO)}{5}
\contentsline {subsubsection}{\numberline {2.2.5}The ring of Moyal-deformed differential operators ($\hbar $-$\Psi $DO)}{6}
\contentsline {subsection}{\numberline {2.3}Inverse Of The Skew-Laurent Series}{6}
\contentsline {subsection}{\numberline {2.4}Problems And Chanllenges For The Fractional Powers Of Skew-Laurent Series}{7}
\contentsline {section}{\numberline {3}Computational Examples}{7}
\contentsline {subsection}{\numberline {3.1}Example: $\Psi $DOs}{7}
\contentsline {subsubsection}{\numberline {3.1.1}Definition and basic operations for $\Psi $DOs}{8}
\contentsline {subsubsection}{\numberline {3.1.2}Fractional powers for $\Psi $DOs.}{10}
\contentsline {subsection}{\numberline {3.2}Example: Shift and Difference Operators And Semi-Discrete Integrable Models}{11}
\contentsline {subsubsection}{\numberline {3.2.1}Definition of difference operator and examples}{11}
\contentsline {subsubsection}{\numberline {3.2.2}Definition of shift operator and examples}{14}
\contentsline {subsection}{\numberline {3.3}Example: $q$-deformed Differential Operators And $q$-deformed Integrable Systems}{16}
\contentsline {subsection}{\numberline {3.4}Example: Moyal-deformed Integrable Systems}{16}
\contentsline {subsubsection}{\numberline {3.4.1}Moyal-deformed KdV equation}{17}
\contentsline {subsubsection}{\numberline {3.4.2}Moyal-deformed KP hierarchy}{17}
\contentsline {subsection}{\numberline {3.5}Example: A Matrix Case}{18}
\contentsline {section}{\numberline {4}Implementation And Description Of {\tt ORESUS} Package }{19}
\contentsline {subsection}{\numberline {4.1}Description of {\tt ORESUS} package}{19}
