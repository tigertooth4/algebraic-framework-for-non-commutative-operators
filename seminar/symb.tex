\documentclass[professionalfonts]{beamer}
\usepackage{amsmath,amsthm,amssymb}
\usepackage{fontspec}
\usepackage{graphicx}
\newtheorem{eg}{Example}

%\setsansfont{Optima}
%\setmainfont{Optima}
\setmonofont{Courier}
\begin{document}
\title{\texttt{FriCAS} and symbolic computation for integrable equations}
\author{Xiaojun Liu, Siqi Zhou, Chunwei Yan}
\begin{frame}
  \titlepage
\end{frame}
\section{Integrable Systems w.r.t. Operators}
\subsection{ Continuous integrable systems}
\begin{frame}
 \frametitle{Continuous integrable systems}
  \begin{eg}[KdV]
    KdV equation $u_t = 6uu_x + u_{xxx}$ has Lax equation:
    \begin{displaymath}
      L_t = [B_3,L]
    \end{displaymath}
    where  $L = \partial^2 + u$, $B_3 = L^{3/2}_+=\partial^3+2u\partial + u_x$.
  \end{eg}
  \begin{eg}[KP]
    KP equation has Lax equation:
    \begin{displaymath}
      L_t = [L^3_+, L]
    \end{displaymath}
    where $L=\partial + u_1\partial^{-1}+u_2\partial^{-2}+\cdots$
  \end{eg}
\end{frame}

\begin{frame}
  Pseudo-differential operator satisfies the Leibniz Law:
  \begin{displaymath}
    \partial \circ f = f\partial + f_x
  \end{displaymath}
  and $\partial^{-1}$ acts by
  \begin{displaymath}
    \partial^{-1} \circ f = \sum_{i=0}^\infty \binom{-1}{i}f^{(i)}\partial^{-1-i}
  \end{displaymath}
  Problem: Hard to compute by hands.
\end{frame}

\subsection{Semi-discrete Integrable Systems}
\begin{frame}
  \frametitle{Semi-discrete Integrable Systems}
  Often represented by \emph{shift op} $\Lambda$ or \emph{difference op} $\Delta$
  \begin{displaymath}
    \Lambda (f(n)) = f(n+1),\quad \Delta(f(n)) = f(n+1)-f(n) = (\Lambda - 1)f(n)
  \end{displaymath}
  Shift op can be used to define Toda Lattice in 1D and 2D cases
  \begin{eg}[2D Toda Lattice Hierarchy]
    \begin{align*}
      L_{x_m}=[L^m_+, L]\quad &\quad L_{y_m} = [M^m_-, L]\\
      M_{x_m}=[L^m_+,M] \quad&\quad M_{y_m} = [M^m_-, M]
    \end{align*}
    where $L= \Lambda + \sum_{i=0}^\infty u_i\Lambda^{-i}$, $M = \sum_{j=-1}^\infty v_j\Lambda^j$.
  \end{eg}
\end{frame}

\begin{frame}
  Difference op can be used to define semi-discrete systems:
  \begin{eg}[Discrete KP]
    Define $L = \Delta + \sum_{j=0}^\infty f_j(n)\Delta^{-j}$, then \emph{discrete KP} equation is
    \begin{displaymath}
      \frac{\partial L}{\partial t_i} = [L^i_+, L]
    \end{displaymath}
  \end{eg}
\end{frame}
\subsection{The q-Deformed Integrable System}
\begin{frame}
  \frametitle{The q-Deformed Integrable System}
  The $q$-deformed derivative $\partial_q$ is defined by
  \begin{displaymath}
    \left(\partial_qf(x)\right) = \dfrac{f(qx)-f(x)}{x(q-1)}
  \end{displaymath}
  And the \emph{$q$-shift op} $\theta$ is defined by $\theta(f(x))=f(qx)$. Further
  \begin{displaymath}
    \partial_q\circ f = (\partial_q f) + \theta(f)\partial_q
  \end{displaymath}
  The $q$-deformed KP hierarchy is given by
  \begin{displaymath}
    \dfrac{\partial L}{\partial t_n} = [L^n_+,L]
  \end{displaymath}
  where $L=\partial_q + \sum_{i=0}^\infty u_i\partial_q^{-i}$.
\end{frame}

\subsection{Moyal-Deformed and Star-Product Integrable System}
\begin{frame}
  \frametitle{Moyal-Deformed and Star-Product Integrable System}
  The \emph{star-product}
  \begin{displaymath}
    A(x,p)\star B(x,p) = e^{\hbar(\partial_x \partial_{\tilde{p}}-\partial_p\partial_{\tilde
        x})}A(x,p)B(\tilde{x},\tilde{p})\bigg|_{\tilde{x}=x,\tilde{p}=p}
  \end{displaymath}
  The conventional \emph{Moyal bracket}
  \begin{displaymath}
    \left\{A(x,p), B(x,p)\right\}_\hbar = \dfrac1{2\hbar}\left(A\star B - B\star A\right)
  \end{displaymath}
  By such rules, we find
  \begin{displaymath}
    p^n\star p^m = p^{n+m},\quad\quad
    p^n\star f(x) = \sum_{m=0}^\infty\binom{n}{m}(-2\hbar)^m f^{(m)}\star p^{n-m}
  \end{displaymath}
\end{frame}
\begin{frame}
  So we consider the Lax operator $L=p^2 + u(x)$, then $L^{3/2}_+ = p^3 + \frac32 u\star p
  -\frac{\hbar}{2}u_x$. Therefor the Moyal-Lax eq
  \begin{displaymath}
    \dfrac{\partial L}{\partial t} = \left\{ L, (L^{3/2}_+)\right\}_\hbar
  \end{displaymath}
  gives $u_t = -(\hbar u^3 + \frac32 u u_x)$
\end{frame}

\section{Skew-Larent Series}
\begin{frame}
  \frametitle{Skew-Larent Series}
  It turns out, all this operations can be considered in an abstract way. Suppose $R$ is a suitable
  ring (in KdV case, the ring is differential polynomial ring of $u(x)$), $x$ is the indeterminate,
  $\sigma:R\longrightarrow R$ is an automorphism on $R$, $\delta:R\longrightarrow R$ is a
  $\sigma$-derivative, namely $$\delta(ab) = \sigma(a)\delta(b) + \delta(a)b.$$
  Therefore, the product of indeterminate $x$ and an element $a\in R$ following the way
  \begin{displaymath}
    x a = \sigma(a) x + \delta(a)
  \end{displaymath}
\end{frame}

\begin{frame}
  \frametitle{Comparison}
  We claim the following operations are special cases of skew-product.
  \begin{center}
  \begin{tabular}{ccccc}
    \hline
    Ring & $x$ & $\sigma$ & $\delta$ & $x a = \sigma(a) x + \delta(a)$ \\
    \hline
    Diff.Poly. & $\partial$ & $1$ & $\partial_x$ & $\partial \circ u = u\partial + u_x$\\
    Differ. Poly. & $\Delta$ & $\Lambda$ & $\Delta$ & $\Delta \circ f = \Lambda(f) \Delta +
    \Delta(f)$\\
    Differ.Poly. & $\Lambda$ & $\Lambda$ & $0$ & $\Lambda\circ f = \Lambda(f) \Lambda$\\
    Diff.Poly. & $\partial_q$ & $\theta$ & $\partial_q$ & $\partial_q\circ u = \theta(u)\partial_q +
    \partial_q(u)$\\
    Diff.Poly. & $p\star$ & $1$ & $-2\hbar\partial_x$ & $p\star u = u\star p + (-2\hbar)u_x$\\
    \hline
  \end{tabular}
\end{center}
So we define the \emph{skew-Laurent ring} to be
  \begin{displaymath}
    R((x;\sigma,\delta)) := \left\{ \sum_{i\ge N} a_i x^{-i} \; \bigg|\; a_i\in R\right\}
  \end{displaymath}
\end{frame}

\begin{frame}
  \frametitle{$x^{-1}a$}
  It is interesting to give a formula for $x^{-1} a$. For doing this, we assume
  \begin{displaymath}
    x^{-1}a = \sum_{i=1}a_i x^{-i}
  \end{displaymath}
  Since $x\cdot x^{-1} a = a$, we can determine recursively all the coefficients $a_i$. Therefore we
  find
  \begin{displaymath}
    x^{-1}a = \sum_{i\ge 1}(-\sigma^{-1}\delta)^{i-1}\sigma^{-1}(a) x^{-i}
  \end{displaymath}
  One can check $R((x;\sigma,\delta))$ is a associative ring (obviously, it is non-commutative).
\end{frame}
\section{FriCAS and Symbolic Computing}
\subsection{An Intro to FriCAS}
\begin{frame}
  \frametitle{What is FriCAS} 
  \begin{figure}[h]
    \centering
    \includegraphics[height=0.2\textheight]{axiom.pdf}
  \end{figure}
  Axiom was born in the mid-70’s as a system called Scratchpad developed by IBM researchers. It is
  not an interactive interpretive environment operating only in response to one line commands—it is
  a complete language with rich syntax and a full compiler. Mathematics can be developed and
  explored with ease by the user of Axiom. In fact, during Axiom’s growth cycle, many detailed
  mathematical domains were constructed.
\end{frame}

\begin{frame}
  On October 1, 2001 Axiom was withdrawn from the market and ended life as a commercial product. On
  September 3, 2002 Axiom was released under the Modified BSD license, including this document. On
  August 27, 2003 Axiom was released as free and open source software available for download from
  the Free Software Foundation’s website, Savannah.
\end{frame}

\begin{frame}
  \frametitle{Feature: {\fontspec{Courier} Stream}}

  {\fontspec{Courier} [i for i in 1.. | prime?(i)]}
  \begin{displaymath}
    [2,3,5,7,11,13,17,19,23,29,...]
  \end{displaymath}
  {\fontspec{Courier}\quad\quad\quad\quad\quad\quad\quad\quad Type: Stream PositiveInteger}
  
\end{frame}

\begin{frame}
  \frametitle{Feature: Lazy and delay computing}
%  \begin{block}
    {\fontspec{Courier} (1)->~ f(0)==1;}\\
    {\fontspec{Courier} (2)->~ f(1)==1;}\\
    {\fontspec{Courier} (3)->~ f(n)==f(n-1)~+~f(n-2);}\\
    {\fontspec{Courier} (4)->~ s:=[f(i) for i in 1..]}
    \begin{displaymath}
      [1,2,3,5,8,13,21,34,55,89,...]
    \end{displaymath}
    {\fontspec{Courier}\quad\quad\quad\quad\quad\quad\quad\quad Type: Stream PositiveInteger}
    {\fontspec{Courier} (5)->~ s.100}
    \begin{displaymath}
      573147844013817084101
    \end{displaymath}
    {\fontspec{Courier}\quad\quad\quad\quad\quad\quad\quad\quad Type: PositiveInteger}
%  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Feature: Operations Can Refer To Abstract Types}
  {\fontspec{Courier} R: Ring
    
    power: (R, NonNegativeInteger): R -> R 
    
    power(x, n) == x ** n}

  \vspace{1cm}
  Then {\fontspec{Courier} power(3,2)} will produce $9$, for $x=3$ and $R=${\fontspec{Courier}
    Integer}. {\fontspec{Courier} power($
    \begin{pmatrix}
      1 & 1\\
      0 & 1
    \end{pmatrix}
    $,2)} will produce $
  \begin{pmatrix}
    1 & 2 \\ 0 & 1
  \end{pmatrix}
  $, for $x=
  \begin{pmatrix}
    1 & 1\\ 0 & 1
  \end{pmatrix}
  $ and $R=${\fontspec{Courier} SquareMatrix(Integer,2)}.
\end{frame}

\section{Integrable Systems and Symbolic Computation}
\begin{frame}
  \frametitle{Package: ORESUS}
  We developped a {\fontspec{Courier} domain} called {\fontspec{Courier}ORESUS}
  (UnivariateSkewSeries), about 460 lines of code. The definition looks like:
  {\fontspec{Courier}
    UnivariateSkewSeries(R:Ring, \\
    ~~~~~~~~~~~~~~~~~~~~ Var:Symbol, \\
    ~~~~~~~~~~~~~~~~~~~~ sigma:Automorphism R, \\
    ~~~~~~~~~~~~~~~~~~~~ invsigma:Automorphism R,\\ 
    ~~~~~~~~~~~~~~~~~~~~ delta: R -> R) 
  }

  We implemented arithmetic operations $+$, $-$, $*$,{\fontspec{Courier} inv}, $n$-th power and
  operations like {\fontspec{Courier} coefficient}, {\fontspec{Courier} generate},
  {\fontspec{Courier} pos},{\fontspec{Courier} neg}, etc.
\end{frame}

\begin{frame}
  \frametitle{Examples: Kupershmidt Deformation}
  We found Kupershmidt deformed KdV equation has a pseudo-differential operator Lax equation:
  \begin{displaymath}
    L_t = [L^{3/2}_+ + R, L]
  \end{displaymath}
  where $L=\partial^2 + u$, $R=(-\frac12 w\partial + \frac14 w_x)L^{-1}$, and $L^{-1}$ can be
  explicitly computed:
  \begin{displaymath}
    L^{-1} = \partial^{-2} -u\partial^{-4} + 2u_x\partial^{-5} +(u^2-3u_{xx})\partial^{-6} +o(\partial^{-6})
  \end{displaymath}
  
\end{frame}

\begin{frame}
  \frametitle{Kupershmidt's Code}
  {\fontspec{Courier}
    R := EXPR (INT)\\
    S : Symbol := 'D\\
    W := (operator 'w)[x]\\
    U := (operator 'u)[x]\\

    sigma : Automorphism(R) := 1\\
    invsigma : Automorphism(R) := 1\\
    delta : R -> R := f +-> D(f,x)\\

    PDO := ORESUS(R,S,sigma,invsigma,delta)\\

    LAX :PDO := generate([1,0,U], 2)\$ PDO\\

    A :PDO := generate([-1/2*W, 1/4*D(W,x)],1)\\
    Deform:= A * inv(LAX)\\
    result:= Deform * LAX - LAX * Deform\\

    coef := coefficient(result, -2)\\
    knownRules := rule (coef == 0)

    [knownRules (coefficient(result, -i)\$ PDO) for i in 1..20]
    
  }
\end{frame}

\begin{frame}
  \frametitle{Discrete KP}
  Let $L=\Delta +u_0+ u_1\Delta^{-1}+u_2\Delta^{-2}+\cdots$, then discrete KP is given by
  \begin{displaymath}
    L_{t_n} = [B_n,L]=[L^n_+,L]
  \end{displaymath}
  And the zero-curvature equation for $B_2$ and $B_3$ gives rise to discrete KP equation:
  \begin{displaymath}
    B_{2,t} - B_{3,y} + [B_2,B_3]=0
  \end{displaymath}
  We calculate this equation
\end{frame}

\begin{frame}
  \frametitle{Discrete KP: Code}
  {\fontspec{Courier}
    R := EXPR (INT); \\
    S : Symbol := 'D;

    shift: R -> R := f +-> eval(f,n,n+1); \\
    invShift: R -> R := f +-> eval(f,n,n-1);
    
    sigma : Automorphism(R) := morphism(shift); \\
    invsigma : Automorphism(R) := morphism(invShift);
    
    delta : R -> R := f +-> shift(f) - f;
    
    DO := ORESUS(R,S,sigma,invsigma,delta)
    
    CoefStream := [operator(subscript('u,[i::OutputForm]))[n] for i in 0..]; \\
    L: DO := generate(CoefStream, 0, minusInfinity())\$ DO + generate([1],1)\$ DO
    
    B2:DO := pos(L**2)\$ DO \\
    B3:DO := pos(L**3)\$ DO

    B2 B3 - B3 B2
  }
\end{frame}
\end{document}
