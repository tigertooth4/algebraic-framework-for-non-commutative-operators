\beamer@endinputifotherversion {3.10pt}
\beamer@sectionintoc {1}{Integrable Systems w.r.t. Operators}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{ Continuous integrable systems}{2}{0}{1}
\beamer@subsectionintoc {1}{2}{Semi-discrete Integrable Systems}{4}{0}{1}
\beamer@subsectionintoc {1}{3}{The q-Deformed Integrable System}{6}{0}{1}
\beamer@subsectionintoc {1}{4}{Moyal-Deformed and Star-Product Integrable System}{7}{0}{1}
\beamer@sectionintoc {2}{Skew-Larent Series}{9}{0}{2}
\beamer@sectionintoc {3}{FriCAS and Symbolic Computing}{12}{0}{3}
\beamer@subsectionintoc {3}{1}{An Intro to FriCAS}{12}{0}{3}
\beamer@sectionintoc {4}{Integrable Systems and Symbolic Computation}{17}{0}{4}
